/*
 * Clase Persona con excepciones
 */
package excepciones;

/**
 *
 * @author mfontana
 */
public class Persona {

    private String nombre;
    private int edad;

    public Persona() {
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) throws PersonaException {
        if (edad > 120 || edad < 0) {
//            PersonaException error = new PersonaException("Edad incorrecta. Debe estar entre 0 y 120");
//            throw error;
               throw new PersonaException("Edad incorrecta debe estar entre 0 y 120");
        } else {
            this.edad = edad;
        }

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", edad=" + edad + '}';
    }

}
