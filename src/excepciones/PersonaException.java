/*
 * Excepciones que puede generar la clase Persona
 */
package excepciones;

/**
 *
 * @author mfontana
 */
public class PersonaException extends Exception {

    public PersonaException(String message) {
        super(message);
    }
    
    
    
}
