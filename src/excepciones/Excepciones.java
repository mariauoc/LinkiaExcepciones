/*
 * Ejemplo con excepciones
 */
package excepciones;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mfontana
 */
public class Excepciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Persona p = new Persona();
            p.setNombre("Mar");
            p.setEdad(200);
            System.out.println(p);
        } catch (PersonaException ex) {
            System.out.println("Error al crear la persona: " + ex.getMessage());
//            Logger.getLogger(Excepciones.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
